package FibonacciProject;

import java.util.stream.IntStream;

/**
 * Class for extract odd and even numbers from array
 * and built Fibonacci numbers
 * @version 1.1 2018-09.18
 * @author Igor Benzel
 */
class Fibonacci {
    private int [] numbers;

    /**
     * @param first the first number in starting array
     * @param last the last number in starting array
     */
    public Fibonacci (int first, int last) {
        int lenth = last-first+1;
        numbers = new int[lenth];
        int counter = first;
        for (int i=0; i<numbers.length; i++){
            numbers [i]=counter;
            counter++;
        }
    }

    public void printNumbers (int [] someNumbers) {
        System.out.print("[");
        for (int i : someNumbers){
            System.out.print(i);
            if (i==someNumbers[someNumbers.length-1]){
                System.out.println("]");
            }else {
                System.out.print(", ");
            }
        }
    }

    public void printNumbersInReverse (int [] someNumbers) {
        System.out.print("[");
        for (int i = someNumbers.length-1; i>=0; i--){
            int number =someNumbers [i];
            System.out.print(number);
            if (number==someNumbers[0]){
                System.out.println("]");
            }else {
                System.out.print(", ");
            }
        }
    }

    private int [] getOddNumbers (int [] someNumbers) {
        int[] oddNumbers;
        oddNumbers = IntStream.of(someNumbers).filter(i->i%2==1).toArray();
        return oddNumbers;
    }

    public  int[] getCurrentOddNumber (){
        return getOddNumbers(numbers);
    }

    private int [] getEvenNumbers (int [] someNumbers) {
        int[] evenNumbers;
        evenNumbers = IntStream.of(someNumbers).filter(i->i%2==0).toArray();
        return evenNumbers;
    }
     public  int [] getCurrentEvenNumbers () {
        return getEvenNumbers(numbers);
     }

    public int getSum  (int [] someNumbers) {
        return IntStream.of(someNumbers).sum();
    }

    public int getSumOddEven () {
        int sum = getSum(getEvenNumbers(numbers))+getSum(getOddNumbers(numbers));
        return  sum;
    }

    public int [] getFibonacciNumbers (int size) {
        int first = IntStream.of(getOddNumbers(numbers)).max().getAsInt();
        int second = IntStream.of(getEvenNumbers(numbers)).max().getAsInt();
        int [] numFibonacci = new int[size];
        numFibonacci [0] = first;
        numFibonacci [1] = second;
        for (int i=2; i<size; i++){
            numFibonacci [i] = numFibonacci[i-2]+numFibonacci[i-1];
         }
         return numFibonacci;
    }

    /**
     *
     * @param size the size of created array of Fibonacci numbers
     * @return the persentage of odd numbers in array
     */
    public int getPersOddFibbNum (int size){
        int[] fibonacciNumbers = getFibonacciNumbers(size);
        float odd = (float)getOddNumbers(fibonacciNumbers).length;
        return (int)((odd/ fibonacciNumbers.length)*100);
    }

    /**
     *
     * @param size the size of created array of Fibonacci numbers
     * @return the persentage of even numbers in array
     */
    public int getPersEvenFibbNum (int size){
        return 100-getPersOddFibbNum(size);
    }
}
