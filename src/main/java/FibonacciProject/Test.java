package FibonacciProject;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class for testing Fibonacci
 * @version 1.1 2018-09.18
 * @author Igor Benzel
 */
public class Test {

    public static void main (String [] args) {
        //Scanner input = new Scanner(System.in);

        System.out.println("Enter first number: ");
        int num1 = insertNumber ();
        System.out.println("First number " + num1);
        System.out.println("Enter last number (must be bigger than first): ");
        int num2;
        do {
            num2 = insertNumber();
            if (num2<=num1) {
                System.out.println("Put the number that is bigger than first " );
            }
        }
        while (num2<=num1);
        System.out.println("Last number " + num2);
        //int second = num2;

        //int first = num1;
        Fibonacci test = new Fibonacci(num1,num2);
        System.out.println("Odd numbers:");
        test.printNumbers(test.getCurrentOddNumber());
        System.out.println("Even numbers in reverse order:");
        test.printNumbersInReverse(test.getCurrentEvenNumbers());
        System.out.println("Sum of odd and even numbers:" +
                test.getSumOddEven());

        System.out.println("Enter size of Fibonacci array: ");

        int fibonacciArraySize;
        do {
            fibonacciArraySize = insertNumber();
            if (fibonacciArraySize<2) {
                System.out.println("Put the number that is bigger than 1 " );
            }
        }
        while (fibonacciArraySize<2);
        System.out.println("Persentage of odd numbers is: " + test.getPersOddFibbNum(fibonacciArraySize));
        System.out.println("Persentage of even numbers is: " + test.getPersEvenFibbNum(fibonacciArraySize));

    }

    private static int insertNumber() {
        Scanner input = new Scanner(System.in);
        boolean continueInput = true;
        int value = 0;
        do {
            try{
                value = input.nextInt();
                continueInput = false;
                System.out.println(
                        "The number entered is " + value);
            }
            catch (InputMismatchException ex) {
                System.out.println("Try again. (" +
                        "Incorrect input: an integer is required)");
                input.nextLine();
            }
        }
        while (continueInput);

        return value;
    }
}
